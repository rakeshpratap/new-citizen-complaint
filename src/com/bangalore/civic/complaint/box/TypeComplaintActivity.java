package com.bangalore.civic.complaint.box;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;

public class TypeComplaintActivity extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_type_complaint);
	}
}
