package com.bangalore.civic.complaint.box;

public class IntentExtraConstants {

	public static final String LATITUDE = "LATITUDE";
	public static final String LONGITUDE = "LONGITUDE";
	public static final String BASE_ITEM_POS = "BASE_ITEM_POS";
	public static final String BASE_ITEM_INDEX = "BASE_ITEM_INDEX";
	public static final String LEVEL_COMPLETE = "LEVEL_COMPLETE";
	public static final String ACTION_DETAILS_POS = "ACTION_DETAILS_POS";
	public static final String ACTION_DETAILS_TEXT = "ACTION_DETAILS_TEXT";

}
